# PHP Code Style (unofficial)
Code style must follow [PSR-1](https://www.php-fig.org/psr/psr-1/) and [PSR-2](https://www.php-fig.org/psr/psr-2/). Generally speaking, everything string-like that's not public-facing should use camelCase. Detailed examples on these are spread throughout the guide in their relevant sections.

## Naming Conventions
- Use camelCase, not underscores, for variable, function and method names, arguments;
- Use underscores for configuration options and parameters;
- Use namespaces for all classes;
- Prefix all abstract classes with Abstract;
- Suffix interfaces with Interface;
- Suffix traits with Trait;
- Suffix exceptions with Exception;
- For type-hinting in PHPDocs and casting, use bool (instead of boolean or Boolean), int (instead of integer), float (instead of double or real);


## Comments
Comments should be avoided as much as possible by writing expressive code. DocBlock variables MUST align at the same position.

```php
// There should be space before a single line comment.

/*
 * If you need to explain a lot you can use a comment block. Notice the
 * single * on the first line. Comment blocks don't need to be three
 * lines long or three characters shorter than the previous line.
 */

/**
 * Constructor
 *
 * @param AcmeClass $acmeClass
 * @param FooClass  $fooClass
 * @param BarClass  $barClass
 */

``` 

## Whitespaces
Statements should have to breathe. In general always add blank lines between statements, unless they're a sequence of single-line equivalent operations. This isn't something enforceable, it's a matter of what looks best in its context.

```php
// Good
public function sampleFunction($sample)
{
    $foo = $this->bar();

    if (! $foo) {
        return null;
    }

    return $foo;
}

// Bad: Everything's cramped together.
public function sampleFunction($sample)
{
    $foo = $this->bar();
    if (! $foo) {
        return null;
    }
    return $foo;
}
```

## Function arguments
When the argument list is split across multiple lines, the closing parenthesis
and opening brace MUST be placed together on their own line with one space
between them.

```php
<?php

namespace Vendor\Package;

class ClassName
{
    public function aVeryLongFunctionName(
        ClassTypeHint $arg1,
        $arg2,
        array $arg3 = []
    ) {
        // ...
    }
}
```