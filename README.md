# {Project name}
> {Short project description}

## Technology stack
List techniques, libraries, frameworks and their used including versions (React, Angular etc...)

- {technique} ({version}) - {description}
- ...

## Installation
Describe how to install the project (locally)

{action}

```bash
$ {command}
```


## Development
### Testing
Describe and show how to run the tests with code examples. Explain what these tests test and why.

## Deployment
Instructions on how to build and release a new version to the server.