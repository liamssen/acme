# Project name
> Put a meaningful, short, plain-language description of what this project is trying to accomplish and why it matters.

## Technology stack
- Laravel (v5.5) - PHP web framework
- React (v1.16) - JavaScript library
- Redis - Caching mechanism

## Installation
Create & configure the `.env` file
```bash
$ cp .env.example .env
```

Install Composer dependencies

```bash
$ php composer.phar install
```

Install the database schema using Doctrine

```bash
$ php app/console doctrine:schema:update --force
```

Install NPM packages

```bash
$ npm install
```

## Development

### Testing
Create a dummy order from creation to shipment
```bash
$ php bin/codecept run order EditCest
```

## Deployment
This project is deployed using Container Deployment with Docker & Kubernetes